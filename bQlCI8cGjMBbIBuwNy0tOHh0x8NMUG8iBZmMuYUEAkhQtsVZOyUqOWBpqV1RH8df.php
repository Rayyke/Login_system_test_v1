<?php
include 'config.php';

session_start();

$username = "";
$name = "";
$surname = "";
$email    = "";
$errors = array(); 

$db = mysqli_connect($db_host, $db_login, $db_password, $db_name);

if (isset($_POST['reg_user'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $surname = mysqli_real_escape_string($db, $_POST['surname']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

    if (empty($username)) { array_push($errors, "Username is required"); }
    if (empty($name)) { array_push($errors, "Name is required"); }
    if (empty($surname)) { array_push($errors, "Surname is required"); }
    if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($password_1)) { array_push($errors, "Password is required"); }
    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }

    $user_check_query = "SELECT * FROM user WHERE username='$username' OR email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    
    if ($user) {
        if ($user['username'] === $username) {
            array_push($errors, "Username already exists");
        }

        if ($user['email'] === $email) {
            array_push($errors, "email already exists");
        }
    }

    if (count($errors) == 0) {
        $password = md5($password_1);

        $query = "INSERT INTO user (username, name, surname, email, password) 
                VALUES('$username', '$name', '$surname', '$email', '$password')";
        mysqli_query($db, $query);
        $_SESSION['username'] = $username;
        $_SESSION['success'] = "You are now logged in";
    
        header('location: index.php');
    }
}

if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    if (count($errors) == 0) {
        $enryptedPassword = md5($password);
        $query = "SELECT * FROM user WHERE username='$username' AND password='$enryptedPassword'";
        $results = mysqli_query($db, $query);

        if (mysqli_num_rows($results) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['success'] = "You are now logged in";
            
            $query = "INSERT INTO log (login, time, type) 
                VALUES('$username', NOW(), 'normal')";
            mysqli_query($db, $query);   

            header('location: index.php');
        }
        else {
            $ds=ldap_connect($ldap_server);
            $dn = "uid=" . $username . ", " . $dn_default;
            ldap_set_option($ds,LDAP_OPT_PROTOCOL_VERSION,3);

            if ($ds) {
                if($bind = @ldap_bind($ds, $dn, $password)){
                    $_SESSION['username'] = $username;
                    $_SESSION['success'] = "You are now logged in";
                    
                    $query = "INSERT INTO log (login, time, type) 
                        VALUES('$username', NOW(), 'ldap')";
                    mysqli_query($db, $query);   

                    header('location: index.php');
                }
                else{
                    array_push($errors, "Wrong username/password combination");
                    
                }
            } 
            else {
                array_push($errors, "Unable to connect to LDAP server.");
            }
        }
    }
}

if (isset($_GET['name'])) {
    session_start();
    $name = $_GET['name'];

    $_SESSION['username'] = $name;
    $_SESSION['success'] = "You are now logged in";
    
    $query = "INSERT INTO log (login, time, type) 
        VALUES('$name', NOW(), 'google')";
    mysqli_query($db, $query);   

    header('location: index.php');
}
?>