<?php include('bQlCI8cGjMBbIBuwNy0tOHh0x8NMUG8iBZmMuYUEAkhQtsVZOyUqOWBpqV1RH8df.php') ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <title>Z3</title>
    </head>

    <body>
        <div class="container-fluid">
            <div>
                <h2>Register</h2>
            </div>
                
            <form method="post" action="register.php">
                <?php include('errors.php'); ?>
                <div>
                    <label>Username</label>
                    <input type="text" name="username">
                </div>
                <div>
                    <label>Name</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <label>Surname</label>
                    <input type="text" name="surname">
                </div>
                <div>
                    <label>Email</label>
                    <input type="email" name="email">
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password_1">
                </div>
                <div>
                    <label>Confirm password</label>
                    <input type="password" name="password_2">
                </div>
                <div>
                    <button type="submit" name="reg_user">Register</button>
                </div>
                <p>
                    Already a member? <a href="login.php">Sign in</a>
                </p>
            </form>
        </div>
    </body>
</html>