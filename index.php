<?php 
include 'config.php';

session_start(); 

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
}

$db = mysqli_connect($db_host, $db_login, $db_password, $db_name);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <title>Home</title>
    </head>

    <body>
        <div class="container-fluid">
            <div>
                <h2>Home Page</h2>
            </div>

            <div>
                <?php if (isset($_SESSION['success'])) : ?>
                    <div>
                        <h3>
                            <?php 
                            echo $_SESSION['success']; 
                            unset($_SESSION['success']);
                            ?>
                        </h3>
                    </div>
                <?php endif ?>

                <?php  if (isset($_SESSION['username'])) : ?>
                    <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
                    <p>
                        Your log:<br>
                        <?php
                            $i = 1;
                            $username = $_SESSION['username'];
                            $query = "SELECT time FROM log WHERE login='$username'";

                            foreach ( $db->query("$query") as $row ) {
                                print $i++ . ") " . $row['time'] . "<br>";
                            }
                        ?>
                    </p>
                    <p>
                        Log statistics:<br>
                        <?php
                            $query = "SELECT COUNT(type) as 'normal' FROM log WHERE type='normal';";
                            $result = mysqli_query($db, $query);
                            $row1 = mysqli_fetch_assoc($result);   

                            $query = "SELECT COUNT(type) as 'ldap' FROM log WHERE type='ldap';";
                            $result = mysqli_query($db, $query);
                            $row2 = mysqli_fetch_assoc($result); 

                            $query = "SELECT COUNT(type) as 'google' FROM log WHERE type='google';";
                            $result = mysqli_query($db, $query);
                            $row3 = mysqli_fetch_assoc($result); 
                        ?>

                        <ul>
                            <li>Normal: <?php echo $row1['normal'] ?></li>
                            <li>LDAP: <?php echo $row2['ldap'] ?></li>
                            <li>Google: <?php echo $row3['google'] ?></li>
                        </ul>  
                    </p>
                    <p> <a href="index.php?logout='1'">logout</a> </p>
                <?php endif ?>
            </div>   
        </div>        
    </body>
</html>