<?php include('bQlCI8cGjMBbIBuwNy0tOHh0x8NMUG8iBZmMuYUEAkhQtsVZOyUqOWBpqV1RH8df.php') ?>

<script>
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        console.log('Name: ' + profile.getName());
        window.location.replace("bQlCI8cGjMBbIBuwNy0tOHh0x8NMUG8iBZmMuYUEAkhQtsVZOyUqOWBpqV1RH8df.php?name=" + profile.getName());

        var id_token = googleUser.getAuthResponse().id_token;
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect();

        $('#google_token').val(id_token); //hidden form value
        $('#google-oauth').submit(); //hidden form
    }                   
</script>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="https://apis.google.com/js/platform.js" async defer></script>
        
        <meta name="google-signin-client_id" content="1084820840885-tesp82mputlq7qcntfbsp7gntc0k1fj1.apps.googleusercontent.com">


        <title>Z3</title>
    </head>
    <body>
        <div class="container-fluid">
            <div>
                <h2>Login</h2>
            </div>
                
            <form method="post" action="login.php">
                <?php include('errors.php'); ?>
                <div>
                    <label>Username</label>
                    <input type="text" name="username" >
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password">
                </div>
                <div>
                    <button type="submit" name="login_user">Login</button>
                </div>
                <p>
                    Not yet a member? <a href="register.php">Sign up</a>
                </p>
                <p>
                    <div class="g-signin2" data-onsuccess="onSignIn"></div>
                </p>
            </form>
        </div>
    </body>
</html>